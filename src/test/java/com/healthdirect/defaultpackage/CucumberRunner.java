package com.healthdirect.defaultpackage;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:target/cucumber" }, features = {
		// "src/test/resources/Feature"
"src/test/resources/feature/verify-search-functionality.feature"
}, 		
glue = {"src/test/java/com.healthdirect.stepdefinition"}
)
public class CucumberRunner {

}
